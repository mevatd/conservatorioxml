/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.CtlDom.getElementEtiqueta;
import static controller.CtlDom.getValorEtiqueta;
import java.util.ArrayList;
import model.Conservatorio;
import model.Instrumento;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author 4710HQ
 */
public class CtlCo {

    //Constants amb els noms de les etiquetes
    static final String ET_INSTRS = "INSTRUMENTOS";
    static final String ET_INSTR = "INSTRUMENTO";
    static final String ET_NOMINSTR = "NOMBRE";
    static final String ET_MATRICULADOS = "MATRICULADOS";

    static final String ET_CODIGO = "CODIGO";
    static final String ET_NOMBRE = "NOMBRE";
    static final String ET_CODPROV = "COD_PROV";
    static final String ET_CODMUNI = "COD_MUNI";

    static final String ET_CO = "CONSERVATORIO";

    public static Conservatorio llegir(Element eCo) {
        String codigo;
        String nombre;
        int codprov;
        int codmuni;

        codigo = getValorEtiqueta(ET_CODIGO, eCo);
        nombre = getValorEtiqueta(ET_NOMBRE, eCo);
        codprov = Integer.parseInt(getValorEtiqueta(ET_CODPROV, eCo));
        codmuni = Integer.parseInt(getValorEtiqueta(ET_CODMUNI, eCo));

        ArrayList<Instrumento> listaInstr = new ArrayList<>();
        String nomInst;
        int matriculados;

        Element elInstrumentos = getElementEtiqueta(ET_INSTRS, eCo);
        NodeList nListaInstrumentos = elInstrumentos.getChildNodes();

        for (int i = 0; i < nListaInstrumentos.getLength(); i++) {
            Element eInstrumento = (Element) nListaInstrumentos.item(i);
            listaInstr.add(llegirInstrument(eInstrumento));
        }

        Conservatorio c = new Conservatorio(codigo, nombre, codprov, codmuni, listaInstr);

        return c;

    }

    public static Instrumento llegirInstrument(Element elInstrumento) {
        Instrumento i = new Instrumento(
                getValorEtiqueta(ET_NOMBRE, elInstrumento),
                Integer.parseInt(getValorEtiqueta(ET_MATRICULADOS, elInstrumento)));
        return i;
    }

    public static void escriure(Conservatorio c, Element elemArrel, Document doc) {

        Element elemCon = doc.createElement(ET_CO);

        Element elemCodigo = doc.createElement(ET_CODIGO);

        Element elemNombre = doc.createElement(ET_NOMBRE);
        Element elemCodProv = doc.createElement(ET_CODPROV);
        Element elemCodMun = doc.createElement(ET_CODMUNI);

        Element elemInstrs = doc.createElement(ET_INSTRS);

        elemArrel.appendChild(elemCon);

        elemCon.appendChild(elemCodigo);
        elemCon.appendChild(elemCodProv);
        elemCon.appendChild(elemCodMun);
        elemCon.appendChild(elemInstrs);

        ArrayList<Instrumento> instrumentos = c.getInstrumentos();
        for (Instrumento i : instrumentos) {
            Element eleminstr = doc.createElement(ET_INSTR);
            Element elemNom = doc.createElement(ET_NOMBRE);
            Element elemMat = doc.createElement(ET_MATRICULADOS);

            elemInstrs.appendChild(eleminstr);
            eleminstr.appendChild(elemNom);
            eleminstr.appendChild(elemMat);

        }

    }
}
