/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.CtlDom.deDocAXml;
import static controller.CtlDom.deXmlADoc;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Node;
import javax.xml.transform.TransformerException;
import model.Conservatorio;
import model.Conservatorios;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author 4710HQ
 */
public class CtlCos {

    static final String ET_COS = "CONSERVATORIOS";
    File file = null;
    Conservatorios cos;

    public CtlCos() {
        this.cos = new Conservatorios();
    }

    public CtlCos(Conservatorios cos) {
        this.cos = cos;
        this.file = new File("defaultXML.xml");
    }

    public CtlCos(Conservatorios cos, File file) {
        this.cos = cos;
        this.file = file;
    }

    public Document recuperar() throws ParserConfigurationException, SAXException, IOException {
        Document doc = null;

        doc = deXmlADoc(file);
        return doc;
    }

    public Document recuperar(File xmlFile) throws ParserConfigurationException, SAXException, IOException {
        Document doc = null;
        doc = deXmlADoc(xmlFile);
        return doc;
    }

    public Conservatorios llegir(Document doc) {
        this.cos.clear();

        Element elCos = doc.getDocumentElement();

        NodeList listaCos = elCos.getChildNodes();

        for (int i = 0; i < listaCos.getLength(); i++) {

            if (listaCos.item(i).getNodeType() == (Node.ELEMENT_NODE)) {

                this.cos.add(CtlCo.llegir((Element) listaCos.item(i)));

            }
        }
        System.out.println("Hay " + cos.size() + " conservatorios");
        return this.cos;
    }

    public void escriure(Document doc) {
        Element arrel = doc.createElement(ET_COS);
        System.out.println();
        doc.appendChild(arrel);
        for (Conservatorio b : cos) {
            CtlCo.escriure(b, arrel, doc);
        }
    }

    public void emmagatzemar(Document doc, File file) throws TransformerException {
        deDocAXml(doc, file);
    }
}
