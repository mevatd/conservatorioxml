/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.CtlCos;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import model.Conservatorio;
import model.Conservatorios;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author 4710HQ
 */
public class View {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {

        //-------------- XML TO DOM ------------------
        //conservatorio donde temporalmente guardamos cada objeto leido
        Conservatorios css = new Conservatorios();

        File fichLectura = new File("conservatorios.xml");

        //DOM que es como el XML pero en "virtual" para q sean procesados sus nodos
        Document docProcesar = null;

        //Control de los conservatorio (el nodo raiz)
        CtlCos ccs = new CtlCos();

        //guardamos las el XML en el DOM
        docProcesar = ccs.recuperar(fichLectura);

        //leemos el arbol de nodos (DOM) uno a uno guardando temporalmente cada objeto (nodo hijo del raiz)
        css = ccs.llegir(docProcesar);

        System.out.println(css.toString());

        //---------------- DOM TO XML ---------------
        File fichEscritura = new File("hehe.xml");
        
        
    }

}
