/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author 4710HQ
 */
public class Conservatorio {

    private final String codigo;
    private String nombre;
    private int cod_prov;
    private int cod_muni;
    private ArrayList<Instrumento> instrumentos;

    public Conservatorio(String codigo, String nombre, int cod_prov, int cod_muni, ArrayList<Instrumento> instrumentos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cod_prov = cod_prov;
        this.cod_muni = cod_muni;
        this.instrumentos = instrumentos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCod_prov() {
        return cod_prov;
    }

    public void setCod_prov(int cod_prov) {
        this.cod_prov = cod_prov;
    }

    public int getCod_muni() {
        return cod_muni;
    }

    public void setCod_muni(int cod_muni) {
        this.cod_muni = cod_muni;
    }

    public ArrayList<Instrumento> getInstrumentos() {
        return instrumentos;
    }

    public void setInstrumentos(ArrayList<Instrumento> instrumentos) {
        this.instrumentos = instrumentos;
    }

    @Override
    public String toString() {
        String instrs = "";
        for (Instrumento i : instrumentos) {
            instrs += "\n" + i + ",";
        }
        instrs += "\n";
        return "\n Conservatorio{" + "codigo=" + codigo + ", nombre=" + nombre + ", cod_prov=" + cod_prov + ", cod_muni=" + cod_muni + ",\n  instrumentos=" + instrs + '}';
    }

}
