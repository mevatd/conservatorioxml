/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 4710HQ
 */
public class Instrumento {

    private String nombre;
    private int matriculados;

    public Instrumento(String nombre, int matriculados) {
        this.nombre = nombre;
        this.matriculados = matriculados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMatriculados() {
        return matriculados;
    }

    public void setMatriculados(int matriculados) {
        this.matriculados = matriculados;
    }

    @Override
    public String toString() {
        return "Instrumento{" + "nombre=" + nombre + ", matriculados=" + matriculados + '}';
    }

}
